Transition path sampling
========================

A generic, multi-core / multi-GPU implementation of the [transition path sampling](https://en.wikipedia.org/wiki/Transition_path_sampling) simulation method for atomic and molecular systems. It relies on the [atooms](https://framagit.org/atooms/atooms.git) framework and on efficient simulation backends (ex. [LAMMPS](http://lammps.sandia.gov/)).

Quick start
-----------
From python:

```python
from atooms.simulation import Simulation
from atooms.backends.lammps import LAMMPS
from atooms.transition_path_sampling import TransitionPathSampling

# Create backends and wrap them as simulation instances
file_input = 'data/lj.xyz'
cmd = """
pair_style      lj/cut 2.5
pair_coeff      1 1 1.0 1.0 2.5
neighbor        0.3 bin
neigh_modify    every 20 delay 0 check no
fix             1 all nve
"""
bck = LAMMPS(file_input, cmd)
sim = Simulation(bck, steps=1000)
tps = TransitionPathSampling(sim, output_path='/tmp/output_dir', steps=10)
tps.run()
```

From the command line:
```shell
tps.py -n 10 -N 1000 --temperature 1.0 --script data/ka_rho1.2.xyz.lammps -i data/ka_rho1.2.xyz /tmp/output
```

Installation
------------
The easiest way to install sample is with pip (coming soon!)
```
pip install atooms-tps
```

Alternately, you can clone the code repository and install from source
```
git clone https://framagit.org/atooms/transition-path-sampling.git
cd transition_path_sampling
make install
```

Contributing
------------
Contributions to the project are welcome. If you wish to contribute, check out [these guidelines](https://framagit.org/atooms/atooms/-/blob/master/CONTRIBUTING.md).

Authors
-------
Daniele Coslovich: https://www.units.it/daniele.coslovich/

Francesco Turci: https://francescoturci.wordpress.com/
